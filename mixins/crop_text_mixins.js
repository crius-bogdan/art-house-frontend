export default {
  filters: {
    crop_title(value) {
      if(!value) return 'Empty field!';

      if(value.length > 34) {
        return value.slice(0, 34) + '...';
      }

      return value;
    }
  }
}
