import Vue from 'vue';
import crop_text_mixins from "~/mixins/crop_text_mixins";

Vue.mixin(crop_text_mixins);
